# Form Filler

Date of development: October 2015

Automatically fills in a form (provided as a Word file) with information from an Excel file.
Language: Python 3.x

You need to install docx, python-docx and xlrd:
pip install docx
pip install python-docx
pip install xlrd

Then you can execute with:
python main.py

When prompted the location of the excel file, give the full path (e.g. C:/Users/user/Desktop/x/lista.xlsx)
When prompted the location of the docx file, give the full path (e.g. C:/Users/user/Desktop/x/sablon.docx)
When prompted about the delimitator, give the character you want replaced in the doc, in our example underscored (which is _)
When prompted the location of the final files, give the full path of the file you want it to be in (e.g. C:/Users/user/Desktop/x/final.docx)
If all goes well, expect to see in the path you gave it (in our example in C:/Users/user/Desktop/x/) all the forms filled.