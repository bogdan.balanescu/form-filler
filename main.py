'''
Created on Apr 26, 2015

@author: Bogdan
'''

from docx import Document
import xlrd

print("Salut!")
print("Acest program va crea un fisier folosind campurile unui excel si un word ca sablon.")
print("Criterii de folosire:")
print("1. Introduceti la cerere locatia completa a Excel-ului.")
print("2. Introduceti la cerere locatia completa a Word-ului.")
print("3. Introduceti la cerere caracterul din sablonul word care reprezinta campurile ce trebuiesc completate.")
print("4. Introduceti la cerere locatia completa urmata de numele fisierului care va contine rezultatul.")

#read excel location
text = input("Locatie Excel:")
print(text)

#open excel
book = xlrd.open_workbook(text,'r')

#get first sheet
first_sheet = book.sheet_by_index(0)

#read docx location
doctext = input("Locatie Word:")
print(doctext)

#open docx
document = Document(doctext)

#iterate through docx paragraphs
alltext = ""
for p in document.paragraphs:
    alltext = alltext + p.text.encode('utf-8').decode() + '\n'

#read delimitator (the character that will be replaced)
delimitator = input("Caracter:")

#eliminate deliminator consecutive duplicates (___ becomes just _)
i = 0
while i <= len(alltext)-1:
    if alltext[i] == delimitator and alltext[i+1] == delimitator:
        alltext = alltext[:i] + alltext[i+1:]
        continue
    i = i+1

#read final docx location (if more documents will be created, numbers will be appended before the file type)
numeFis = input("Locatie document final:")

def therightvalue(text):
    if isinstance(text, int):
        return 'int'
    elif isinstance(text, float):
        return 'float'
    else:
        return 'str'

#create a final document for each row in the excel's first sheet
for i in range(first_sheet.nrows):
    #append a number to each final document
    sl = numeFis.rfind("/")
    bsl = numeFis.rfind("\\")
    maxpoint = sl if sl > bsl else bsl
    efname = numeFis[maxpoint+1:]
    dotocc = efname.find(".")
    numeDoc=numeFis[:maxpoint+dotocc+1] + str(i) + numeFis[maxpoint+dotocc+1:]
    
	#create a new document and open it with a different identifier
    document.save(numeDoc)
    newdocument = Document(numeDoc)
	
	#clear the document's content
    for paragraph in newdocument.paragraphs:
        paragraph.clear()
    
    p = newdocument.paragraphs[0]
    
	#duplicate the original's document content
    textcopy = alltext[:]
	
	#replace the delimitators with the row's data
    row = first_sheet.row_values(i)
    for s in row:
        j = textcopy.find(delimitator)
        if therightvalue(s) in ['float']:
            if s%1 == 0:
                textcopy = textcopy[:j] + str(int(s)) + textcopy[j+1:]
            else:
                textcopy = textcopy[:j] + str(s) + textcopy[j+1:]
        else:
            textcopy = textcopy[:j] + str(s) + textcopy[j+1:]
    p.text = textcopy
    
    #save the document
    newdocument.save(numeDoc)